﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class GameInfo
    {
        protected int[,] Player1 = new int[10, 10];
        protected int[,] Player2 = new int[10, 10];
        protected int pointPlayer, pointEnemy;

        public void NewGame()
        {
            pointPlayer = pointEnemy = 0;
            for(int i = 0; i < 10; i++)
                for(int j = 0; j < 10; j++)
                {
                    Player1[i, j] = 0;
                    Player2[i, j] = 0;
                }
            bool fl;
            int w4 = 1, w3 = 2, w2 = 3, w1 = 4, choose, rotate = 0, x, y;
            Random rnd = new Random();
            do
            {
                fl = false;
                switch (rotate)
                {
                    case 1: w1++;
                        break;
                    case 2: w2++;
                        break;
                    case 3: w3++;
                        break;
                    case 4: w4++;
                        break;
                }
                if (w4 == 0 && w3 == 0 && w2 == 0 && w1 == 0)
                    break;
                choose = rnd.Next(1, 5);
                rotate = rnd.Next(0, 2);
                if (rotate == 0)
                    switch (choose)
                    {
                        case 1:
                            if (w1 != 0)
                            {
                                fl = true;
                                w1--;
                                x = rnd.Next(0, 10);
                                y = rnd.Next(0, 10);
                                PlaceWarship(x, y, choose, ref rotate, "enemy");
                            }
                            break;
                        case 2:
                            if (w2 != 0)
                            {
                                fl = true;
                                w2--;
                                x = rnd.Next(0, 10);
                                y = rnd.Next(0 + 1, 10);
                                PlaceWarship(x, y, choose, ref rotate, "enemy");
                            }
                            break;
                        case 3:
                            if (w3 != 0)
                            {
                                fl = true;
                                w3--;
                                x = rnd.Next(0, 10);
                                y = rnd.Next(0 + 2, 10);
                                PlaceWarship(x, y, choose, ref rotate, "enemy");
                            }
                            break;
                        case 4:
                            if (w4 != 0)
                            {
                                fl = true;
                                w4--;
                                x = rnd.Next(0, 10);
                                y = rnd.Next(0 + 3, 10);
                                PlaceWarship(x, y, choose, ref rotate, "enemy");
                            }
                            break;
                    }
                if (rotate == 1 && fl == false)
                    switch (choose)
                    {
                        case 1:
                            if (w1 == 0)
                            {
                                rotate = 0;
                                break;
                            }
                            if (w1 != 0)
                            {
                                w1--;
                                x = rnd.Next(0, 10);
                                y = rnd.Next(0, 10);
                                PlaceWarship(x, y, choose, ref rotate, "enemy");
                            }
                            break;
                        case 2:
                            if (w2 == 0)
                            {
                                rotate = 0;
                                break;
                            }
                            if (w2 != 0)
                            {
                                w2--;
                                x = rnd.Next(0 + 1, 10);
                                y = rnd.Next(0, 10);
                                PlaceWarship(x, y, choose, ref rotate, "enemy");
                            }
                            break;
                        case 3:
                            if (w3 == 0)
                            {
                                rotate = 0;
                                break;
                            }
                            if (w3 != 0)
                            {
                                w3--;
                                x = rnd.Next(0 + 2, 10);
                                y = rnd.Next(0, 10);
                                PlaceWarship(x, y, choose, ref rotate, "enemy");
                            }
                            break;
                        case 4:
                            if (w4 == 0)
                            {
                                rotate = 0;
                                break;
                            }
                            if (w4 != 0)
                            {
                                w4--;
                                x = rnd.Next(0 + 3, 10);
                                y = rnd.Next(0, 10);
                                PlaceWarship(x, y, choose, ref rotate, "enemy");
                            }
                            break;
                    }
            } while (true);
        }
        public void PlaceWarship(int x, int y, int wn, ref int rotate, string who)
        {
            if (who == "player")
            {
                if (y > wn - 2 && rotate == 0)
                {
                    int a = x == 0 ? 0 : x - 1, b;
                    for (; a <= x + 1 && a < 10; a++)
                    {
                        b = y == 0 ? 0 : y - wn;
                        for (; b >= y - wn && b <= y + 1 && b < 10; b++)
                        {
                            if (b < 0)
                                continue;
                            if (Player1[a, b] == 1)
                            {
                                rotate = wn;
                                return;
                            }
                        }
                    }
                    for (int i = wn; i > 0; i--)
                        Player1[x, y - (i - 1)] = 1;
                    a = x == 0 ? 0 : x - 1;
                    for (; a <= x + 1 && a < 10; a++)
                    {
                        b = y == 0 ? 0 : y - wn;
                        for (; b >= y - wn && b <= y + 1 && b < 10; b++)
                        {
                            if (b < 0)
                                continue;
                            if (Player1[a, b] == 1)
                                continue;
                            Player1[a, b] = 2;
                        }
                    }
                    return;
                }
                if (x > wn - 2 && rotate == 1)
                {
                    int a = x == 0 ? 0 : x - wn, b;
                    for (; a <= x + 1 && a < 10; a++)
                    {
                        b = y == 0 ? 0 : y - 1;
                        for (; b >= y - wn && b <= y + 1 && b < 10; b++)
                        {
                            if (a < 0)
                                continue;
                            if (Player1[a, b] == 1)
                            {
                                rotate = wn;
                                return;
                            }
                        }
                    }
                    for (int i = wn; i > 0; i--)
                        Player1[x - (i - 1), y] = 1;
                    a = x == 0 ? 0 : x - wn;
                    for (; a <= x + 1 && a < 10; a++)
                    {
                        b = y == 0 ? 0 : y - 1;
                        for (; b >= y - wn && b <= y + 1 && b < 10; b++)
                        {
                            if (a < 0)
                                continue;
                            if (Player1[a, b] == 1)
                                continue;
                            Player1[a, b] = 2;
                        }
                    }
                    rotate = 0;
                    return;
                }
            }
            if (who == "enemy")
            {
                if (y > wn - 2 && rotate == 0)
                {
                    int a = x == 0 ? 0 : x - 1, b;
                    for (; a <= x + 1 && a < 10; a++)
                    {
                        b = y == 0 ? 0 : y - wn;
                        for (; b >= y - wn && b <= y + 1 && b < 10; b++)
                        {
                            if (b < 0)
                                continue;
                            if (Player2[a, b] == 1)
                            {
                                rotate = wn;
                                return;
                            }
                        }
                    }
                    for (int i = wn; i > 0; i--)
                        Player2[x, y - (i - 1)] = 1;
                    a = x == 0 ? 0 : x - 1;
                    for (; a <= x + 1 && a < 10; a++)
                    {
                        b = y == 0 ? 0 : y - wn;
                        for (; b >= y - wn && b <= y + 1 && b < 10; b++)
                        {
                            if (b < 0)
                                continue;
                            if (Player2[a, b] == 1)
                                continue;
                            Player2[a, b] = 2;
                        }
                    }
                    return;
                }
                if (x > wn - 2 && rotate == 1)
                {
                    int a = x == 0 ? 0 : x - wn, b;
                    for (; a <= x + 1 && a < 10; a++)
                    {
                        b = y == 0 ? 0 : y - 1;
                        for (; b >= y - wn && b <= y + 1 && b < 10; b++)
                        {
                            if (a < 0)
                                continue;
                            if (Player2[a, b] == 1)
                            {
                                rotate = wn;
                                return;
                            }
                        }
                    }
                    for (int i = wn; i > 0; i--)
                        Player2[x - (i - 1), y] = 1;
                    a = x == 0 ? 0 : x - wn;
                    for (; a <= x + 1 && a < 10; a++)
                    {
                        b = y == 0 ? 0 : y - 1;
                        for (; b >= y - wn && b <= y + 1 && b < 10; b++)
                        {
                            if (a < 0)
                                continue;
                            if (Player2[a, b] == 1)
                                continue;
                            Player2[a, b] = 2;
                        }
                    }
                    rotate = 0;
                    return;
                }
            }
            rotate = wn;
        }
        public int Enemy(int x, int y)
        {
            if (pointEnemy == 20 || pointPlayer == 20)
                return -2;
            if (CheckHit("enemy", x, y))
                return 1;
            return CheckPlacement(x, y);
        }
        public int CheckPlacement(int i, int j)
        {
            return Player1[i, j];
        }
        public bool CheckHit(string who, int i, int j)
        {
            if (who == "enemy" && Player1[i, j] == 1)
            {
                pointEnemy++;
                Player1[i, j] = -1;
                return true;
            }
            if (who == "player" && Player2[i, j] == 1)
            {
                pointPlayer++;
                Player2[i, j] = -1;
                return true;
            }
            return false;
        }
        public string GameOver()
        {
            if (pointPlayer > 19)
                return "player";
            if (pointEnemy > 19)
                return "enemy";
            return "";
        }
    }
}
