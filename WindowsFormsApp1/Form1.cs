﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using ClassLibrary1;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        PictureBox[,] PlayerArea = new PictureBox[10, 10];
        PictureBox[,] EnemyArea = new PictureBox[10, 10];
        GameInfo game = new GameInfo();
        int one, two, start = -1, rotate = 0, shipkind = 1, w1, w2, w3, w4, turn;
        public Form1()
        {
            InitializeComponent();
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    PlayerArea[i, j] = new PictureBox();
                    PlayerArea[i, j].Size = new Size(24, 23);
                    PlayerArea[i, j].BackColor = Color.Cyan;
                    PlayerArea[i, j].Location = new Point((i * 26) + 31, (j * 25) + 32);
                    PlayerArea[i, j].MouseHover += Hovered;
                    PlayerArea[i, j].MouseLeave += OnLeave;
                    PlayerArea[i, j].Enabled = true;
                    PlayerArea[i, j].Visible = true;
                    PlayerArea[i, j].Tag = i * 10 + j;
                    EnemyArea[i, j] = new PictureBox();
                    EnemyArea[i, j].Tag = i * 10 + j;
                    EnemyArea[i, j].BackColor = Color.Cyan;
                    EnemyArea[i, j].Size = new Size(24, 23);
                    EnemyArea[i, j].Location = new Point((i * 26) + 31, (j * 25) + 32);
                    EnemyArea[i, j].Click += Chosen;
                    EnemyArea[i, j].Enabled = false;
                    EnemyArea[i, j].Visible = false;
                    EnemyArea[i, j].SendToBack();
                    Controls.Add(PlayerArea[i, j]);
                    Controls.Add(EnemyArea[i, j]);
                }
            }
        }
        void OnLeave(object sender, EventArgs e)
        {
            if (start == 0)
            {
                for (int i = 0; i < 10; i++)
                    for (int j = 0; j < 10; j++)
                        PlayerArea[i, j].BackColor = Color.Cyan;
            }
        }
        void Hovered(object sender, EventArgs e)
        {
            PictureBox change = sender as PictureBox;
            if (start == 0)
            {
                FindIndex(change);
                if (two > shipkind - 2 && rotate == 0)
                {
                    for (int i = shipkind; i > 0; i--)
                        if (game.CheckPlacement(one, two - (i - 1)) == 0)
                            PlayerArea[one, two - (i - 1)].BackColor = Color.Green;
                    int a = one == 0 ? 0 : one - 1, b;
                    for (; a <= one + 1 && a < 10; a++)
                    {
                        b = two == 0 ? 0 : two - shipkind;
                        for (; b >= two - shipkind && b <= two + 1 && b < 10; b++)
                        {
                            if (b < 0)
                                continue;
                            if (game.CheckPlacement(a, b) > 0 || PlayerArea[a, b].BackColor == Color.Green)
                                continue;
                            PlayerArea[a, b].BackColor = Color.Gray;
                        }
                    }
                    return;
                }
                if (one > shipkind - 2 && rotate == 1)
                {
                    for (int i = shipkind; i > 0; i--)
                        if (game.CheckPlacement(one - (i - 1), two) == 0)
                            PlayerArea[one - (i - 1), two].BackColor = Color.Green;
                    int a = one == 0 ? 0 : one - shipkind, b;
                    for (; a <= one + 1 && a < 10; a++)
                    {
                        b = two == 0 ? 0 : two - 1;
                        for (; b >= two - shipkind && b <= two + 1 && b < 10; b++)
                        {
                            if (a < 0)
                                continue;
                            if (game.CheckPlacement(a, b) > 0 || PlayerArea[a, b].BackColor == Color.Green)
                                continue;
                            PlayerArea[a, b].BackColor = Color.Gray;
                        }
                    }
                }
            }
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.R && rotate == 0)
            {
                rotate = 1;
                return;
            }
            if (e.KeyCode == Keys.R && rotate == 1)
            {
                rotate = 0;
                return;
            }
            if (e.KeyCode == Keys.D1)
            {
                shipkind = 1;
                return;
            }
            if (e.KeyCode == Keys.D2)
            {
                shipkind = 2;
                return;
            }
            if (e.KeyCode == Keys.D3)
            {
                shipkind = 3;
                return;
            }
            if (e.KeyCode == Keys.D4)
            {
                shipkind = 4;
            }
        }

        void Chosen(object sender, EventArgs e)
        {
            bool fl = false;
            PictureBox change = sender as PictureBox;
            FindIndex(change);
            if (start == 0)
            {
                fl = false;
                if (rotate == 0)
                    switch (shipkind)
                    {
                        case 1:
                            if (w1 != 0)
                            {
                                fl = true;
                                w1--;
                                game.PlaceWarship(one, two, shipkind, ref rotate, "player");
                            }
                            break;
                        case 2:
                            if (w2 != 0)
                            {
                                fl = true;
                                w2--;
                                game.PlaceWarship(one, two, shipkind, ref rotate, "player");
                            }
                            break;
                        case 3:
                            if (w3 != 0)
                            {
                                fl = true;
                                w3--;
                                game.PlaceWarship(one, two, shipkind, ref rotate, "player");
                            }
                            break;
                        case 4:
                            if (w4 != 0)
                            {
                                fl = true;
                                w4--;
                                game.PlaceWarship(one, two, shipkind, ref rotate, "player");
                            }
                            break;
                    }
                if (rotate == 1 && fl == false)
                    switch (shipkind)
                    {
                        case 1:
                            if (w1 == 0)
                            {
                                rotate = 0;
                                break;
                            }
                            if (w1 != 0)
                            {
                                w1--;
                                game.PlaceWarship(one, two, shipkind, ref rotate, "player");
                            }
                            break;
                        case 2:
                            if (w2 == 0)
                            {
                                rotate = 0;
                                break;
                            }
                            if (w2 != 0)
                            {
                                w2--;
                                game.PlaceWarship(one, two, shipkind, ref rotate, "player");
                            }
                            break;
                        case 3:
                            if (w3 == 0)
                            {
                                rotate = 0;
                                break;
                            }
                            if (w3 != 0)
                            {
                                w3--;
                                game.PlaceWarship(one, two, shipkind, ref rotate, "player");
                            }
                            break;
                        case 4:
                            if (w4 == 0)
                            {
                                rotate = 0;
                                break;
                            }
                            if (w4 != 0)
                            {
                                w4--;
                                game.PlaceWarship(one, two, shipkind, ref rotate, "player");
                            }
                            break;
                    }
                switch (rotate)
                {
                    case 1:
                        w1++;
                        rotate = 0;
                        break;
                    case 2:
                        w2++;
                        rotate = 0;
                        break;
                    case 3:
                        w3++;
                        rotate = 0;
                        break;
                    case 4:
                        w4++;
                        rotate = 0;
                        break;
                }
                if (w4 == 0 && w3 == 0 && w2 == 0 && w1 == 0)
                {
                    textBox1.Text = "Ваша черга";
                    for (int i = 0; i < 10; i++)
                        for (int j = 0; j < 10; j++)
                        {
                            PlayerArea[i, j].Enabled = PlayerArea[i, j].Visible = false;
                            EnemyArea[i, j].Enabled = EnemyArea[i, j].Visible = true;
                        }
                    for (int i = 0; i < 10; i++)
                        for (int j = 0; j < 10; j++)
                        {
                            PlayerArea[i, j].Click -= Chosen;
                            PlayerArea[i, j].BackColor = Color.Cyan;
                        }
                    turn = 1;
                    start = 1;
                    fl = true;
                }
            }
            if (start == 1 && fl == false && turn == 1)
            {
                if (change.BackColor == Color.Gray || change.BackColor == Color.Red)
                    return;
                if (game.CheckHit("player", one, two))
                {
                    change.BackColor = Color.Red;
                    if (game.GameOver() == "player")
                    {
                        for (int i = 0; i < 10; i++)
                            for (int j = 0; j < 10; j++)
                                PlayerArea[i, j].Click -= Chosen;
                        start = -1;
                        textBox1.Text = "Ви виграли!";
                    }
                    return;
                }
                textBox1.Text = "Натисніть праву кнопку щоб продовжити";
                change.BackColor = Color.Gray;
                button2.Enabled = true;
                turn = 0;
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (turn == 0)
            {
                textBox1.Text = "Хід супротивника\r\nНатисніть праву кнопку щоб продовжити";
                for (int i = 0; i < 10; i++)
                    for (int j = 0; j < 10; j++)
                    {
                        if (PlayerArea[i, j].BackColor != Color.Red)
                            PlayerArea[i, j].Enabled = true;
                        PlayerArea[i, j].Visible = true;
                        EnemyArea[i, j].Enabled = EnemyArea[i, j].Visible = false;
                    }
                EnemyTurn();
                return;
            }
            button2.Enabled = false;
            textBox1.Text = "Ваша черга";
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                {
                    PlayerArea[i, j].Enabled = PlayerArea[i, j].Visible = false;
                    if (EnemyArea[i, j].BackColor != Color.Red)
                        EnemyArea[i, j].Enabled = true;
                    EnemyArea[i, j].Visible = true;
                }

        }
        private void button1_Click(object sender, EventArgs e)
        {
            start = 0;
            w4 = 1;
            w3 = 2;
            w2 = 3;
            w1 = 4;
            textBox1.Text = "";
            game.NewGame();
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                {
                    EnemyArea[i, j].BackColor = Color.Cyan;
                    PlayerArea[i, j].BackColor = Color.Cyan;
                }
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                {
                    PlayerArea[i, j].Click -= Chosen;
                    PlayerArea[i, j].Click += Chosen;
                    PlayerArea[i, j].Enabled = PlayerArea[i, j].Visible = true;
                    EnemyArea[i, j].Enabled = EnemyArea[i, j].Visible = false;
                }
        }
        void FindIndex(PictureBox box)
        {
            one = two = 0;
            int find = int.Parse(box.Tag.ToString());
            while (find > 9)
            {
                find -= 10;
                one += 1;
            }
            two = find;
        }
        void EnemyTurn()
        {
            Random rnd = new Random();
            int x, y, hit;
            if (PlayerArea[0, 0].Enabled == true)
            {
                for (; ; )
                {
                    x = rnd.Next(0, 10);
                    y = rnd.Next(0, 10);
                    hit = game.Enemy(x, y);
                    if (hit == -2)
                        break;
                    if (hit == 2 && PlayerArea[x, y].BackColor == Color.Gray)
                        continue;
                    if (hit == 0 && PlayerArea[x, y].BackColor != Color.Gray || hit == 2)
                    {
                        PlayerArea[x, y].BackColor = Color.Gray;
                        break;
                    }
                    if (hit == 1)
                        PlayerArea[x, y].BackColor = Color.Red;
                }
                if (game.GameOver() == "enemy")
                {
                    button2.Enabled = false;
                    for (int i = 0; i < 10; i++)
                        for (int j = 0; j < 10; j++)
                            PlayerArea[i, j].Click -= Chosen;
                    start = -1;
                    textBox1.Text = "Ви програли!";
                    return;
                }
            }
            turn = 1;
        }
    }
}
